#NOTE: will not run as-is; need to fill in myUsername and myPass fields

import sys
from splinter import Browser
import pandas as pd
import time
import getpass

myUser = raw_input("Enter username: ")
#will hide password
myPass = getpass.getpass("Enter password: ")

#run headless
browser = Browser('chrome', headless=True)

#go to login page
browser.visit('https://login.stonehill.edu/cas/login?service=https%3A%2F%2Fget.cbord.com%2Fstonehill%2Ffull%2Flogin.php')

userField_X = '//*[@id="username"]'
userField = browser.find_by_xpath(userField_X)[0]
#fill in 'myUsername' with user's id
userField.fill(myUser)

passField_X = '//*[@id="password"]'
passField = browser.find_by_xpath(passField_X)[0]
#fill in 'myPass' with user's password
passField.fill(myPass)

loginBtn_X = '//*[@id="fm1"]/div[4]/input[4]'
loginBtn = browser.find_by_xpath(loginBtn_X)[0]
loginBtn.click()

#wait for page to render
time.sleep(1)

overview_X = '//*[@id="get_funds_overview"]'
overview = browser.find_by_xpath(overview_X)[0].value

recent_X = '//*[@id="my_recent_transactions"]/div'
recent = browser.find_by_xpath(recent_X)[0].value


print "\n Get Funds Overview \n"
print overview
print "\n Get Recent Transactions \n"
print recent


#now go to all transactions

transRedir_X = '//*[@id="my_recent_transactions"]/a[1]'
transRedir = browser.find_by_xpath(transRedir_X)[0]
transRedir.click()

time.sleep(1)

allTrans_X = '//*[@id="historyTable"]/table'
allTrans = browser.find_by_xpath(allTrans_X)[0].value


print "\n All Transactions \n"
print allTrans

#send overview, recent, allTrans to new method
