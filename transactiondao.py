import dataset
import logging
from user import User
from flask import current_app
from transaction import Transaction
import time
from datetime import datetime
from decimal import Decimal
from flask import session
from datetime import timedelta
from datetime import date
#for browser
from splinter import Browser
import pandas as pd
import time
import getpass

class TransactionDao:
    def __init__(self):
        self.connectString = 'sqlite:///transactions.db'
        self.db = dataset.connect(self.connectString)
        self.table = self.db['transactions']
        try:
            self.logger = current_app.logger
        except:
            self.logger = logging.getLogger('root')

    def percentThrough(self):
        #initialize datetime object for start of semester
        #NOTE: date is hardcoded
        start = date(2018,1,14)
        end = date(2018, 5, 12)

        delta = end - start

        #current day
        currentDT = datetime.now()
        currentDate = date(currentDT.year,currentDT.month,currentDT.day)
        #subtract start of semester from now
        delta1 = currentDate - start

        num1 = int(delta.days)
        num2 = int(delta1.days)

        num3 = num2/float(num1)
        #get number of days left in semester
        daysLeft = delta - delta1

        #get percent through semester
        percentLeft = num3*100

        return {'daysLeft':daysLeft, 'percentLeft':percentLeft} 

    #calculate how much money total is left in the account
    def getTotalMoneyLeft(self, overviewArray):
        money = []
        for line in overviewArray:
            #self.logger.debug(line)
            if(line[0]=='$'):
                money.append(line.replace('$',''))

        totalMoneyLeft = 0
        for pos in money:
            totalMoneyLeft = totalMoneyLeft + float(pos)

        return totalMoneyLeft

    #calculate how much you should spend over the remainder of the semester based on your current balance
    def getTotalToSpend(self, daysLeft, moneyLeft):
        return float(moneyLeft)/float(daysLeft)
    
    def mealArray(self, allTrans):
        table = self.readTransactions(allTrans)
        transactionArray = self.loadArray(table)

        breakfast = []
        lunch = []
        dinner = []
        afterDinner = []

        #will take a day and add all transactions for each meal into one transaction object
        #EX: if a person buys a meal and then goes back to buy a drink we don't want to count that as two transactions, we want it to be one.
        #will give us more accurate averages
        size = len(transactionArray)
        if transactionArray[0]=='Breakfast':
            breakfast.append(transactionArray[0])
        elif transactionArray[0]=='Lunch':
            lunch.append(transactionArray[0])
        elif transactionArray[0]=='Dinner':
            dinner.append(transactionArray[0])
        elif transactionArray[0]=='After Dinner':
            afterDinner.append(transactionArray[0])
        #will check the transaction before to see if it is on the same day and it's the same meal, if it is, then we combine them.
        for x in range(1, size):
            date = transactionArray[x].datetime.day
            if transactionArray[x].meal=='Breakfast':
                if (transactionArray[x-1].datetime.day == date and transactionArray[x-1].meal=='Breakfast'):
                    transactionArray[x-1].amt = transactionArray[x-1].amt + transactionArray[x].amt
                else:
                    breakfast.append(transactionArray[x])
            elif transactionArray[x].meal=='Lunch':
                if (transactionArray[x-1].datetime.day == date and transactionArray[x-1].meal=='Lunch'):
                    transactionArray[x-1].amt = transactionArray[x-1].amt + transactionArray[x].amt
                else:                        
                    lunch.append(transactionArray[x])
            elif transactionArray[x].meal=='Dinner':
                if (transactionArray[x-1].datetime.day == date and transactionArray[x-1].meal=='Dinner'):
                    transactionArray[x-1].amt = transactionArray[x-1].amt + transactionArray[x].amt
                else:                         
                    dinner.append(transactionArray[x])
            elif transactionArray[x].meal=='After Dinner':
                if (transactionArray[x-1].datetime.day == date and transactionArray[x-1].meal=='After Dinner'):
                    transactionArray[x-1].amt = transactionArray[x-1].amt + transactionArray[x].amt
                else:
                    afterDinner.append(transactionArray[x])

        #get transactions of each meal
        #for trans in transactionArray:
        #    if trans.meal=='Breakfast':
        #        breakfast.append(trans)
        #    elif trans.meal=='Lunch':
        #        lunch.append(trans)
        #    elif trans.meal=='Dinner':
        #        dinner.append(trans)
        #    elif trans.meal=='After Dinner':
        #        afterDinner.append(trans)

        #calculate cost spend at each mealtime
        #breakfast
        breakfastTotal = 0
        for a in breakfast:
            breakfastTotal = breakfastTotal + a.amt
        #lunch
        lunchTotal = 0
        for b in lunch:
            lunchTotal = lunchTotal + b.amt
        #dinner
        dinnerTotal = 0
        for c in dinner:
            dinnerTotal = dinnerTotal + c.amt
        #afterDinner
        afterDinnerTotal = 0
        for d in afterDinner:
            afterDinnerTotal = afterDinnerTotal + d.amt
        

        #compute averages: total of each meal divided by length of meal arrays
        avgBreakfast = breakfastTotal/len(breakfast)
        avgLunch = lunchTotal/len(lunch)
        avgDinner = dinnerTotal/len(dinner)
        avgAfterDinner = afterDinnerTotal/len(afterDinner)
            
        return {'breakfast':breakfast, 'lunch':lunch, 'dinner':dinner, 'afterDinner':afterDinner, 'breakfastTotal':breakfastTotal, 'lunchTotal':lunchTotal, 'dinnerTotal':dinnerTotal, 'afterDinnerTotal':afterDinnerTotal, 'avgBreakfast':avgBreakfast, 'avgLunch':avgLunch, 'avgDinner':avgDinner, 'avgAfterDinner':avgAfterDinner}
    
    #day input is mm/dd/yyyy, this method converts to datetime format
    def selectByDate(self, day, allTrans):
        #self.logger.debug('----new day ----')
        month,day,year = day.split('/')
        time = '12:00PM'
        theDate = month + ' ' + day + ' ' + year + ' ' + time
        
        date_object = datetime.strptime(theDate, '%m %d %Y %I:%M%p')
        

        table = self.readTransactions(allTrans)
        #array of transactions for this user
        transactionArray = self.loadArray(table)

        seeTrans = []
        for t in transactionArray:
            m = t.datetime.month
            d = t.datetime.day
            y = t.datetime.year

            if ((date_object.month==m) and (date_object.day==d) and (date_object.year==y)):
                seeTrans.append(t)
        return seeTrans
        
    def readTransactions(self, allTrans):
        #store all of the data in 2d array
        transTable0 = []
        for line in allTrans.splitlines():
            transTable1 = []
            for line1 in line.split():
                transTable1.append(line1)
            transTable0.append(transTable1)

        self.loadArray(transTable0)

        return transTable0

    #table is 2D array of all transactions
    def loadArray(self, table):
        
        transactionArray = []
        for line in table:
            #spring meal plan
            if line[0]=='Spring':
                form = 'Spring Meal Plan'
                    
                month = line[3]
                #datetime format requires three letter abbrev for each month
                month = month[:3]
                day = line[4].replace(',','')
                year = line[5].replace(',','')
                time = line[6]
                amt = 0
                count = 0
                for word in line:
		    
                    if ('$' in str(word) and ((line[7]!='Linda') and (line[7]!='No') and (line[7]!='Server'))):
                        amt = word.replace('$','')
                        amt = Decimal(amt)
                        theDate = month + ' ' + day + ' ' + year + ' ' + time
                        #self.logger.debug(theDate)
                        dateTime = datetime.strptime(theDate, '%b %d %Y %I:%M%p')
                        meal = self.getTimeOfDay(dateTime)
                        transactionArray.append(Transaction(form,dateTime,amt,meal))               
                        
                    count = count + 1
                    
                

	    #fall meal plan
            if line[0]=='Fall':
                form = 'Fall Meal Plan'
                    
                month = line[3]
                #datetime format requires three letter abbrev for each month
                month = month[:3]
                day = line[4].replace(',','')
                year = line[5].replace(',','')
                time = line[6]
                amt = 0
                count = 0
                for word in line:
		    
                    if ('$' in str(word) and ((line[7]!='Linda') and (line[7]!='No') and (line[7]!='Server'))):
                        amt = word.replace('$','')
                        amt = Decimal(amt)
                        theDate = month + ' ' + day + ' ' + year + ' ' + time
                        #self.logger.debug(theDate)
                        dateTime = datetime.strptime(theDate, '%b %d %Y %I:%M%p')
                        meal = self.getTimeOfDay(dateTime)
                        transactionArray.append(Transaction(form,dateTime,amt,meal))
                    count = count + 1
                       
            #Hill Dollars
            elif line[0]=='Hill':
		form = 'Hill Dollars'
                    
                month = line[2]
                #datetime format requires three letter abbrev for each month
                month = month[:3]
                day = line[3].replace(',','')
                year = line[4].replace(',','')
                time = line[5]
                amt = 0
		count = 0
                for word in line:
		    
                    if ('$' in str(word) and ((line[6]!='Linda') and (line[6]!='No') and (line[6]!='Server'))):
                        amt = word.replace('$','')
                        amt = Decimal(amt)
                        theDate = month + ' ' + day + ' ' + year + ' ' + time
                        #self.logger.debug(theDate)
                        dateTime = datetime.strptime(theDate, '%b %d %Y %I:%M%p')
                        meal = self.getTimeOfDay(dateTime)
                        transactionArray.append(Transaction(form,dateTime,amt, meal))
                        
                    count = count + 1    

            #food flex dollars
            elif line[0]=='Food':
                form = 'Food Flex'
                month = line[3]
                #datetime format requires three letter abbrev for each month
                month = month[:3]
                day = line[4].replace(',','')
                year = line[5].replace(',','')
                time = line[6]
                amt = 0
		count = 0
		for word in line:
		    
                    if ('$' in str(word) and (line[count-1]=='-')):
                        amt = word.replace('$','')
                    count = count + 1
                        
                amt = Decimal(amt)
                theDate = month + ' ' + day + ' ' + year + ' ' + time
                dateTime = datetime.strptime(theDate, '%b %d %Y %I:%M%p')
                meal = self.getTimeOfDay(dateTime)
                transactionArray.append(Transaction(form,dateTime,amt,meal))
                      
  
                    
        return transactionArray

    #method to determine what meal a transaction is a part of
    def getTimeOfDay(self, datetimePassed):
        dateTime = datetimePassed.time()
        begBreak = datetime.strptime('7:30AM', '%I:%M%p')
        endBreak = datetime.strptime('11:30AM', '%I:%M%p')
        begLunch = datetime.strptime('11:31AM', '%I:%M%p')
        endLunch = datetime.strptime('3:00PM', '%I:%M%p')
        begDin = datetime.strptime('3:01PM', '%I:%M%p')
        endDin = datetime.strptime('8:00PM', '%I:%M%p')
        
        #self.logger.debug('time: ' + str(dateTime.hour) + ' ' + str(dateTime.minute))
        if(dateTime>=begBreak.time() and dateTime<=endBreak.time()):
            return 'Breakfast'
        elif(dateTime>=begLunch.time() and dateTime<endLunch.time()):
            return 'Lunch'
        elif(dateTime>=begDin.time() and dateTime<=endDin.time()):
            return 'Dinner'
        else:
            return 'After Dinner'

    #method to render cboard website and get data
    #INPUT: username and password for cboard account
    #OUTPUT: overview, recent transactions, all transactions, 2D array of all transactions
    def getData(self, myUser,myPass):
        #run headless
	browser = Browser('chrome', headless=True)

	#go to login page
	browser.visit('https://login.stonehill.edu/cas/login?service=https%3A%2F%2Fget.cbord.com%2Fstonehill%2Ffull%2Flogin.php')

	userField_X = '//*[@id="username"]'
	userField = browser.find_by_xpath(userField_X)[0]
	#fill in 'myUsername' with user's id
	userField.fill(myUser)
        
	passField_X = '//*[@id="password"]'
	passField = browser.find_by_xpath(passField_X)[0]
	#fill in 'myPass' with user's password
	passField.fill(myPass)

	loginBtn_X = '//*[@id="fm1"]/div[4]/input[4]'
	loginBtn = browser.find_by_xpath(loginBtn_X)[0]
	loginBtn.click()

	#wait for page to render
	time.sleep(1)

	overview_X = '//*[@id="get_funds_overview"]'
	overview = browser.find_by_xpath(overview_X)[0].value
        
	recent_X = '//*[@id="my_recent_transactions"]/div'
	recent = browser.find_by_xpath(recent_X)[0].value

	#now go to all transactions

	transRedir_X = '//*[@id="my_recent_transactions"]/a[1]'
	transRedir = browser.find_by_xpath(transRedir_X)[0]
	transRedir.click()
        
	time.sleep(1)
        
	allTrans_X = '//*[@id="historyTable"]/table'
	allTrans = browser.find_by_xpath(allTrans_X)[0].value

    	return {'overview':overview, 'recent':recent, 'all':allTrans}
