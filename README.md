# Student Meal Plan Budget Estimator

Author: Brian Bloom and Nate Nadu

Student Meal Plan Budget Estimator: AWS EC2 hosted, web-based application through which students can login to check
meal plan activity, as well as meal time breakdown of spending habits and future predictions of balancing spending based off
past habits. Program uses a Python back end which scrapes C-Board’s transaction history with JavaScript and Selenium
browser emulator to generate a HTML front end portal.
