#!/usr/bin/env python

from jsonpickle import encode
from flask import Flask
from flask import abort, redirect, url_for
from flask import request
from flask import render_template
from flask import session
import sys
import logging
from logging.handlers import RotatingFileHandler
from logging import Formatter
from userdao import UserDao
from user import User
from transactiondao import TransactionDao
import time
from splinter import Browser
import pandas as pd
import getpass
from datetime import datetime

app = Flask(__name__)

FORMAT = "[%(filename)s:%(lineo)s - %(funcName)10s() ] %(message)s"
logging.basicConfig(filename='output.log',level=logging.DEBUG)
logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login',methods=['POST', 'GET'])
def login():
    error = None

    if request.method == 'POST':
        userid = request.form['userid']
        password = request.form['password']
        #if isValidLogin(userid,password):
        session['userid'] = encode(userid)
        session['password'] = encode(password)
        return loading()
        #else:
            #error = 'Invalid userid/password'

    return render_template('login.html',error=error)

def isValidLogin(userid, password):
    dao = UserDao()
    user = dao.selectByUserid(str(userid))

    if (user is not None) and (userid== user.userid) and (password == user.password):
        session['user']=encode(user) #holds userid and password
        return True
    else:
        return False


#Signup will link their account on our service
#to the Stonehill account so we can snag info 
@app.route('/signup', methods=['POST', 'GET'])
def signup():
    error = None

    if request.method == 'POST': #if they click sumbit
        userid = request.form['userid']
        password = request.form['password']

        if isValidSignUp(userid): #if the username is available
            dao = UserDao()
            dao.insert(User(userid, password))
            return login()
        else:
            error = 'invalid userid'
    return render_template('signup.html',error=error)

def isValidSignUp(userid): #check to see if username is available to use
    dao = UserDao()
    user = dao.selectByUserid(str(userid))

    if ( (user is not None) and (userid == user.userid) ):
        #if the user exist and the usernames are equal
        return False
    return True

@app.route('/loading',methods=['POST','GET'])
def loading():
    
    return render_template('loading.html')

@app.route('/data',methods=['POST','GET'])
def data():
    myUser = session['userid'].replace('"','')
    myPass = session['password'].replace('"','')

    dao = TransactionDao()
    try:
        data = dao.getData(myUser,myPass)
    except Exception:
        return login()
    overview = data['overview']
    recent = data['recent']
    allTrans = data['all']
                
    session['overview'] = overview
    session['recent'] = recent
    session['allTrans'] = allTrans

    return welcome()

@app.route('/welcome', methods=['POST','GET'])
def welcome():
    username = session['userid'].replace('"','')
    
    dao = TransactionDao()
    overview = session['overview']
    recent = session['recent']
    allTrans = session['allTrans']

    progData = dao.percentThrough()
    percentData = progData['percentLeft']
    daysData = progData['daysLeft']

    #percentage of year completed rounded to two decimal places
    progressPercent = round(percentData, 2)
    #days of the semester remaining
    progressDays = str(daysData)
    progressDays = progressDays.replace(' days, 0:00:00','')

    #only show relevant data for account overview
    overview = overview.split()
    overview = overview[6:25]

    #get total money left in account
    totalMoneyLeft = dao.getTotalMoneyLeft(overview)

    totalToSpend = round(dao.getTotalToSpend(progressDays, totalMoneyLeft), 2)
    
    genData = dao.mealArray(allTrans)
    breakfastTotal = genData['breakfastTotal']
    lunchTotal = genData['lunchTotal']
    dinnerTotal = genData['dinnerTotal']
    afterDinnerTotal = genData['afterDinnerTotal']
    entireTotal = round(float(breakfastTotal) + float(lunchTotal) + float(dinnerTotal) + float(afterDinnerTotal),2)

    #percent spent on breakfast, lunch...
    percentbreakfast = round(float(breakfastTotal)/float(entireTotal),2)
    percentlunch = round(float(lunchTotal)/float(entireTotal),2)
    percentdinner = round(float(dinnerTotal)/float(entireTotal),2)
    percentafterdinner = round(float(afterDinnerTotal)/float(entireTotal),2)

    #calculate how much money to spend/day at each meal
    spendDayBreakfast = round(percentbreakfast*totalToSpend, 2)
    spendDayLunch = round(percentlunch*totalToSpend, 2)
    spendDayDinner = round(percentdinner*totalToSpend,2)
    spendDayAfterDinner = round(percentafterdinner*totalToSpend, 2)
    #round function rounds to two decimal places
    avgBreakfast = str(round(genData['avgBreakfast'], 2))
    avgLunch = str(round(genData['avgLunch'], 2))
    avgDinner = str(round(genData['avgDinner'], 2))
    avgAfterDinner = str(round(genData['avgAfterDinner'], 2))
    
    if ('viewDayTrans' in request.form) and (request.form['datepicker']!=''):
        day = request.form['datepicker']
        seeTrans = dao.selectByDate(day, allTrans)
        return viewDay(day, seeTrans)
    elif ('viewAllTrans' in request.form):
        return viewAllTrans(allTrans)                            

    return render_template('welcome.html', **locals())

@app.route('/viewAllTrans', methods=['POST','GET'])
def viewAllTrans(allTrans):
    return render_template('alltransactions.html', **locals())

@app.route('/viewDay', methods=['POST','GET'])
def viewDay(day, seeTrans):
    total = 0
    for line in seeTrans:
        total = total + line.amt

    breakfastTotal = 0
    lunchTotal = 0
    dinnerTotal = 0
    afterDinnerTotal = 0
    for t in seeTrans:
        if t.meal=='Breakfast':
            breakfastTotal = breakfastTotal + t.amt
        elif t.meal=='Lunch':
            lunchTotal = lunchTotal + t.amt
        elif t.meal=='Dinner':
            dinnerTotal = dinnerTotal + t.amt
        elif t.meal=='After Dinner':
            afterDinnerTotal = afterDinnerTotal + t.amt

    return render_template('pickday.html', **locals())


if __name__ == "__main__":
    app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    streamhandler = logging.StreamHandler(sys.stderr)
    streamhandler.setLevel(logging.DEBUG)
    streamhandler.setFormatter(Formatter("[%(filename)s:%(lineno)s - %(funcName)10s() ] %(message)s"))
    app.logger.addHandler(streamhandler)
    app.logger.setLevel(logging.DEBUG)
    app.run(host='0.0.0.0')
