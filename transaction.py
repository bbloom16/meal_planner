class Transaction:
    def __init__(self, form, datetime, amt, meal):
        #form is type of money : hill dollars, flex, meal plan
        self.form = form
        self.datetime = datetime
        self.amt = amt
        self.meal = meal
        
    def toString(self):
        return "Purchase Type: " + self.form + " Timestamp: " + str(self.datetime) +  " Amount: $" + str(self.amt)
